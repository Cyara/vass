import { createStore } from 'vuex'

export default createStore({
  state: {
    galleryList:[]
  },
  getters: {
  },
  mutations: {
    loadingData: (state, event) => state.galleryList = event
  },
  actions: {
  },
  modules: {
  }
})

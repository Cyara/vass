import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "bootstrap/dist/css/bootstrap.min.css";
import 'bootstrap-icons/font/bootstrap-icons.css'
import axios from 'axios'
import VueAxios from 'vue-axios'

const app = createApp(App)

app.use(store).use(router).mount('#app')
app.use(VueAxios, axios)
import "bootstrap/dist/js/bootstrap.min";
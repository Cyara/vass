import { createRouter, createWebHistory } from 'vue-router'
import AppPage from '@/pages/App.vue'
import BrandingPage from '@/pages/Branding.vue'
import PhotographyPage from '@/pages/Photography.vue'
import WebPage from '@/pages/Web.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName:"home" */ "@/pages/Home.vue")
  },
  {
    path: '/app',
    name: 'app',
    component: AppPage
  },
  {
    path: '/branding',
    name: 'branding',
    component: BrandingPage
  },
  {
    path: '/photography',
    name: 'photography',
    component: PhotographyPage
  },
  {
    path: '/web',
    name: 'web',
    component: WebPage
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
